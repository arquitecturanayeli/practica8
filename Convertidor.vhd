library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned. all;

entity Convertidor is
port(
		B: in std_logic_vector(3 downto 0);
		Q:out std_logic_vector(6 downto 0));
end;

architecture Behavioral of Convertidor is
constant D0: std_logic_vector(6 downto 0):= "1000000";
constant D1: std_logic_vector(6 downto 0):= "1001111";
constant D2: std_logic_vector(6 downto 0):= "0100100";
constant D3: std_logic_vector(6 downto 0):= "0110000";
constant D4: std_logic_vector(6 downto 0):= "0011001";
constant D5: std_logic_vector(6 downto 0):= "0010010";
constant D6: std_logic_vector(6 downto 0):= "0000010";
constant D7: std_logic_vector(6 downto 0):= "1111000";
constant D8: std_logic_vector(6 downto 0):= "0000000";
constant D9: std_logic_vector(6 downto 0):= "0011000";

begin
process (B) begin
	case B is
		when "0000" =>Q<=D0;
		when "0001" =>Q<=D1;
		when "0010" =>Q<=D2;
		when "0011" =>Q<=D3;
		when "0100" =>Q<=D4;
		when "0101" =>Q<=D5;
		when "0110" =>Q<=D6;
		when "0111" =>Q<=D7;
		when "1000" =>Q<=D8;
		when "1001" =>Q<=D9;
	when others =>Q<="0110110";
	end case;
end process;
end Behavioral;
