library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

entity Registro is
    Port ( D : in  STD_LOGIC_VECTOR (7 downto 0);
           CLK,CLR,LA,EA : in  STD_LOGIC;
           Q : out  STD_LOGIC_VECTOR (7 downto 0));
end Registro;

architecture Behavioral of Registro is
signal aux: std_logic_vector(7 downto 0);
begin
	process(CLK,CLR) begin
		if(CLR = '1') then
			aux<="00000000";
			elsif(rising_edge(CLK)) then
				if(LA='1' and EA='0') then
					aux<=D;
					elsif(LA='0' and EA='1') then
						aux <= to_stdlogicvector(to_bitvector(aux) srl 1);
					end if;
				end if;

	end process;
	Q<=aux;
end Behavioral;

