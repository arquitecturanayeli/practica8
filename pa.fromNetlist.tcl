
# PlanAhead Launch Script for Post-Synthesis pin planning, created by Project Navigator

create_project -name Practica8 -dir "/home/carlos/Documentos/ESCOM/Arquitectura/practica8/planAhead_run_3" -part xc6slx16csg324-3
set_property design_mode GateLvl [get_property srcset [current_run -impl]]
set_property edif_top_file "/home/carlos/Documentos/ESCOM/Arquitectura/practica8/UnidadControl.ngc" [ get_property srcset [ current_run ] ]
add_files -norecurse { {/home/carlos/Documentos/ESCOM/Arquitectura/practica8} }
set_param project.pinAheadLayout  yes
set_property target_constrs_file "UnidadControl.ucf" [current_fileset -constrset]
add_files [list {UnidadControl.ucf}] -fileset [get_property constrset [current_run]]
link_design
