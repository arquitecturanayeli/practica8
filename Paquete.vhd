library IEEE;
use IEEE.STD_LOGIC_1164.all;

package Paquete is
component Contador is
    Port ( LB, EB, CLK, CLR : in  STD_LOGIC;
           B : inout  STD_LOGIC_VECTOR (3 downto 0));
end component;

component Convertidor is
    Port ( B : in  STD_LOGIC_VECTOR (3 downto 0);
           Q : out  STD_LOGIC_VECTOR (6 downto 0));
end component;

component Registro is
    Port ( LA, EA,CLK,CLR : in STD_LOGIC;
		Q : out  STD_LOGIC_VECTOR (7 downto 0);
		D : in  STD_LOGIC_VECTOR (7 downto 0));

end component;

end Paquete;

