library IEEE;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;
library work;
use work.Paquete.all;
use IEEE.STD_LOGIC_1164.ALL;

entity UnidadControl is
    Port (
			  clr : in  STD_LOGIC;
           ini  : in  STD_LOGIC;    
			  aux_D:in std_logic_vector(7 downto 0);
			  Q:out std_logic_vector(6 downto 0);	
			  AN: inout STD_LOGIC_VECTOR (3 downto 0);			  
			  clk: in std_logic
			  
			  );
end UnidadControl;

architecture Behavioral of UnidadControl is
signal aux : std_logic_vector(3 downto 0);
signal aux_Q : std_logic_vector(7 downto 0);
signal cont : STD_LOGIC_VECTOR (30 downto 0);
signal la :  STD_LOGIC;
signal ea :  STD_LOGIC;
signal lb :   STD_LOGIC;
signal eb :   STD_LOGIC;
signal a0 :   STD_LOGIC;
signal z :   STD_LOGIC;
signal ec :   STD_LOGIC;

--==============
--ESTADOS
	type estados is (d0,d1,d2);
	signal edo_presente,edo_futuro: estados;
	
signal clk2 : STD_LOGIC;
begin
an <= "0000";


Elemento1 : Contador Port map
	( LB => LB,
		EB => EB,
		CLK => clk2,
		CLR => clr,
      B => aux
		);
		 
Conv1 : Convertidor port map (
	B => aux,
   Q => Q
	);
	
Elemento2 : Registro Port map
	( LA => LA,
		EA => EA,
		CLK => CLK2,
		CLR => CLR,
		Q => aux_Q,
		D => aux_D
		);
 Z <= not(aux_Q(0) or aux_Q(1) or aux_Q(2) or aux_Q(3) or aux_Q(4) or aux_Q(5) or aux_Q(6) or aux_Q(7));


--================================
--ESTADOS
proceso1: process(edo_presente) begin

--primer estado
	case edo_presente is
		when d0=> LB <= '1';
				IF(INI = '1') THEN
				edo_futuro<=d1;
				ELSE
					la<= '1';
					edo_futuro<=d0;
				end if;
--segundo estado
		when d1=> ea <='1';
		if (z = '0') then
			if (a0='1') then
				lb <='1';
				edo_futuro <=d1;
			else
				edo_futuro<=d1;
			end if;
		else
			edo_futuro<= d2;
		end if;
--tercer estado
		when d2=> ec <= '1';
	if (ini = '1') then
		edo_futuro <=d2;
	else
		edo_futuro<=d0;
	end if;

	end case;
	
end process proceso1;

--================================
--divisor
process(clk, clr)
begin
	if (clr = '1') then
		cont <= (others=> '0');
	elsif(clk'event and clk = '1') then
		cont <= cont + 1;
	end if;
end process;

clk2<= cont(25);
--================================
--asignacion de estados
	--Proceso de transicion
	process (clk2,clr)
	begin
		if (clr='1')then
			edo_presente <= d0;
			elsif (rising_edge (clk2)) then
			edo_presente <= edo_futuro; 
		end if;
	end process;
--================================
end Behavioral;

