library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.Numeric_std.all;
use IEEE.STD_LOGIC_unsigned.ALL;

entity Contador is
    Port (B : inout  STD_LOGIC_VECTOR (3 downto 0);
           LB : in  STD_LOGIC;
           EB : in  STD_LOGIC;
			  CLR: in STD_LOGIC;
           CLK : in  STD_LOGIC);
end Contador;

architecture Behavioral of Contador is

begin

	proceso1:process (LB,EB,CLK,CLR,B) 
	variable aux_b: integer;
	begin
		if CLR= '1' then
			B<="0000";
		elsif CLK = '1' then
			if (LB = '1' and EB = '0' )then
				B<="0000";
			elsif (LB = '0' and EB = '1') then
				aux_b:= CONV_INTEGER(B);
				aux_b:= aux_b +1;
				B <= CONV_STD_LOGIC_VECTOR( aux_b, 4 );	
			end if;
		end if;
	
	end process proceso1;
	

	
	

end Behavioral;

