
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

entity Divisor is
    Port ( clk,clr : in  STD_LOGIC;
           clk2 : out std_logic
			  );
end Divisor;

architecture Behavioral of Divisor is
signal cont : STD_LOGIC_VECTOR (30 downto 0);
begin

process(clk, clr)
begin

	if (clr = '1') then
		cont <= (others=> '0');
	elsif(clk'event and clk = '1') then
		cont <= cont + 1;
	end if;
end process;

clk2<= cont(25);

end Behavioral;


